package com.example.akshay.seiedutrust;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;

public class FormActivity extends AppCompatActivity implements AsyncInterface {
    EditText firstName,lastName,rank,indos,cdcNo,passportNo,proficiancy,cocNo,nationality,employer,perAddress,currAddress,email,phone,bloodGroup,kinName,kinRelation,kinPhone,certNo,instituteName,indosNo;
    DatePicker dateOfApplication,datecdcNo,datePassport,dateBirthDay;
    RadioButton allergicYes,allergicNo,paymentCash;
    CheckBox otherCourse,disclamer;
    ImageView profile;
    TextView textViewApplicationDate ,textViewCDCNoDate,textViewDatePassport,textViewDateBirthDay;
    String strFirstName,strLastName,strRank,strIndos,strCdcNo,strPassportNo,strProficiancy,strCocNo,StrNationality,strEmployeer,strPerAdd,strCorrAdd,strEmail,strPhone,strBloodGroup,strKinName,strKinRelation,strKinPhone,strCertNo,strInstitute,strIndoNo;
    String profileImageName,serverImagePath;
    boolean allergicMedicine,payment,OldStudent,SighnedDisclamer;
    String applicationDate,cdcDate,passportDate,birthdate,id;
    private int year, month, day,date,uploaded;
    private static final int SELECT_PICTURE = 100;
    MyLoopjTask myLoopjTask;
    JSONObject data;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        String responce= getIntent().getStringExtra("Data");
        try {
            JSONObject preData = new JSONObject(responce);
            id = preData.getString("ID");
        }catch (JSONException e) {
            e.printStackTrace();
        }

        firstName = (EditText)findViewById(R.id.editTextFirstName);
        lastName = (EditText)findViewById(R.id.editTextLastName);
        rank = (EditText)findViewById(R.id.editTextRank);
        indos = (EditText)findViewById(R.id.editTextInDos);
        cdcNo = (EditText)findViewById(R.id.editTextCDCNo);
        passportNo = (EditText)findViewById(R.id.editTextPassportNo);
        proficiancy = (EditText)findViewById(R.id.editTextProficiancy);
        cocNo = (EditText)findViewById(R.id.editTextCOCNo);
        nationality = (EditText)findViewById(R.id.editTextNationality);
        employer = (EditText)findViewById(R.id.editTextEmployer);
        perAddress = (EditText)findViewById(R.id.editTextPermanentAddress);
        currAddress = (EditText)findViewById(R.id.editTextCurrentAddress);
        email = (EditText)findViewById(R.id.editTextEmail);
        phone = (EditText)findViewById(R.id.editTextPhone);
        bloodGroup = (EditText)findViewById(R.id.editTextBloodGroup);
        kinName = (EditText)findViewById(R.id.editTextKinName);
        kinRelation = (EditText)findViewById(R.id.editTextRelation);
        kinPhone = (EditText)findViewById(R.id.editTextRelationPhone);
        certNo = (EditText)findViewById(R.id.editTextCertNo);
        instituteName= (EditText)findViewById(R.id.editTextCertIssuedBy);
        indosNo = (EditText)findViewById(R.id.editTextCertINDOSNo);
        allergicYes = (RadioButton)findViewById(R.id.radioButtonYes);
        allergicNo = (RadioButton)findViewById(R.id.radioButtonNo);
        paymentCash = (RadioButton)findViewById(R.id.radioButtonCash);
        otherCourse = (CheckBox)findViewById(R.id.CheckBoxCourse);
        disclamer = (CheckBox)findViewById(R.id.CheckBoxDisclamer);
        profile = (ImageView)findViewById(R.id.ImageViewProfile);
        textViewApplicationDate = (TextView)findViewById(R.id.textViewApplicationDate);
        textViewCDCNoDate = (TextView)findViewById(R.id.textViewCDCNoDate);
        textViewDatePassport = (TextView)findViewById(R.id.textViewDatePassport);
        textViewDateBirthDay  = (TextView)findViewById(R.id.textViewDateBirthDay);
        myLoopjTask = new MyLoopjTask();
        myLoopjTask.delegte = this;
    }

    public  void  submitForm(View view) {

        Toast.makeText(getApplicationContext(), "This form is still under development" , Toast.LENGTH_LONG).show();
        strFirstName = firstName.getText().toString();
        strLastName = lastName.getText().toString();
        strRank = rank.getText().toString();
        strIndos =  indos.getText().toString();
        strCdcNo = cdcNo.getText().toString();
        strPassportNo = passportNo.getText().toString();
        strProficiancy = proficiancy.getText().toString();
        strCocNo = cocNo.getText().toString();
        StrNationality = nationality.getText().toString();
        strEmployeer = employer.getText().toString();
        strPerAdd = perAddress.getText().toString();
        strCorrAdd = currAddress.getText().toString();
        strEmail = email.getText().toString();
        strPhone = phone.getText().toString();
        strBloodGroup = bloodGroup.getText().toString();
        strKinName = kinName.getText().toString();
        strKinRelation = kinRelation.getText().toString();
        strKinPhone = kinPhone.getText().toString();
        strCertNo = certNo.getText().toString();
        strInstitute = instituteName.getText().toString();
        strIndoNo = indosNo.getText().toString();
        if(allergicYes.isChecked())
            allergicMedicine = true;
        else
            allergicMedicine = false;
        payment = paymentCash.isChecked();
        OldStudent = otherCourse.isChecked();
        SighnedDisclamer = disclamer.isChecked();
        applicationDate = textViewApplicationDate.getText().toString();
        cdcDate = textViewCDCNoDate.getText().toString();
        passportDate = textViewDatePassport.getText().toString();
        birthdate = textViewDateBirthDay.getText().toString();

        data = new JSONObject();
        try {
            data.put("ID",id);
            data.put("Image",serverImagePath);
            data.put("Surname",strFirstName);
            data.put("OtherNames",strLastName);
            data.put("DateofApplication",applicationDate);
            data.put("RankDesignations",strRank);
            data.put("InDoSNo",strIndos);
            data.put("CDCNo",strCdcNo);
            data.put("CDCDateofIssue",cdcDate);
            data.put("CDCValid",false);
            data.put("PassportNo",strPassportNo);
            data.put("PassportDateofIssue",passportDate);
            data.put("PassportValid",false);
            data.put("CertOfCompletency",strProficiancy);
            data.put("COCNo",strCocNo);
            data.put("COCNoValid",false);
            data.put("DateofBirth",birthdate);
            data.put("Nationality",StrNationality);
            data.put("Employer",strEmployeer);
            data.put("PermanentAddress",strPerAdd);
            data.put("PresentAddress",strCorrAdd);
            data.put("Email",strEmail);
            data.put("PhoneNumber",strPhone);
            data.put("BloodGroup",strBloodGroup);
            data.put("Allergic",allergicMedicine);
            data.put("AllergicDetails",allergicMedicine);
            data.put("Nextofkin",strKinName);
            data.put("Relationtoself",strKinRelation);
            data.put("NoofCert",strCertNo);
            data.put("IssuedBy",strInstitute);
            data.put("IssuedINDosNo",strIndoNo);
            data.put("PaymentMode",1);
            data.put("PaymentRemark","");
            data.put("IsPreviousStudent",false);
            data.put("IsPreviousStudent",false);
            data.put("AcceptForm",true);
            data.put("EmergencyPhoneNumber",strKinPhone);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        SharedPreferences settings = getSharedPreferences(LoginActivity.PREFS_NAME, MODE_PRIVATE);
        RequestParams requestParams = new RequestParams();
        requestParams.put("_formWebModel", data);
        requestParams.put("_courseid", id);
        requestParams.put("key",settings.getString("key", ""));
        myLoopjTask.executeLoopjCallPostSecond(requestParams,Utility.ApplyFormUrl);
    }

    public  void  uploadDate(View view){
        showDialog(999);
        if(view.getId() == R.id.textViewApplicationDate)
            date = 1;
        else if(view.getId() == R.id.textViewCDCNoDate)
            date = 2;
        else  if (view.getId() == R.id.textViewDatePassport){
            date = 3;
        }
        else if(view.getId() == R.id.textViewDateBirthDay){
            date = 4;
        }
    }
    public  void  uploadImage(View view) {
        openImageChooser();
        uploaded =1;
    }
    /* Choose an image from Gallery */
    void openImageChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                // Get the url from data
                Uri selectedImageUri = data.getData();
                if (null != selectedImageUri) {
                    // Get the path from the Uri
                    profileImageName = getPathFromURI(selectedImageUri);
                    Log.i("SEIDuTrust", "Image Path : " + profileImageName);
                    // Set the image in ImageView
                    profile.setImageURI(selectedImageUri);
                    RequestParams requestParams = new RequestParams();
                    try {
                        requestParams.put("uploaded_file", new File(selectedImageUri.getPath()));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    myLoopjTask.executeLoopjCallPostSecond(requestParams,Utility.UploadImageUrl);
                }
            }
        }
    }

    /* Get the real path from the URI */
    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }
    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this,
                    myDateListener, year, month, day);
        }
        return null;
    }
    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    showDate(arg1, arg2+1, arg3);
                }
            };

    private void showDate(int year, int month, int day) {
        if(date ==1) {
            textViewApplicationDate.setText(new StringBuilder().append(day).append("/")
                    .append(month).append("/").append(year));
        }
        else if(date == 2) {
            textViewCDCNoDate.setText(new StringBuilder().append(day).append("/")
                    .append(month).append("/").append(year));
        }
        else if (date == 3){
            textViewDatePassport.setText(new StringBuilder().append(day).append("/")
                    .append(month).append("/").append(year));
        }
        else if (date == 4){
            textViewDateBirthDay.setText(new StringBuilder().append(day).append("/")
                    .append(month).append("/").append(year));
        }
    }

    @Override
    public void processFinish(String output) {

        String responseString = output;
        Log.d(Utility.TAG,responseString);
        if(uploaded == 1) {
            try {
                JSONObject loginOutParams = new JSONObject(responseString);

                if (loginOutParams.getBoolean(Utility.ResponseSuccess)) {
                    serverImagePath = loginOutParams.getString(Utility.ResponseDataModel);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
