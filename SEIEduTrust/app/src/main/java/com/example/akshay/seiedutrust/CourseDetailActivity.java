package com.example.akshay.seiedutrust;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;

public class CourseDetailActivity extends AppCompatActivity implements AsyncInterface {

    String courseDate;
    private View mProgressView;
    JSONObject dateData;
    MyLoopjTask myLoopjTask;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_detail);
        mProgressView = findViewById(R.id.course_progress);
        TextView txtViewCourseName = (TextView) findViewById(R.id.textViewCourseName);
        TextView txtViewCourseAbout = (TextView) findViewById(R.id.textViewAbout);
        TextView txtViewDocuments = (TextView) findViewById(R.id.textViewDocumentslist);
        TextView txtViewDuration= (TextView) findViewById(R.id.textViewDuration);
        TextView txtViewEligibilty = (TextView) findViewById(R.id.textViewEligibilitylist);
        TextView txtViewNoDates = (TextView) findViewById(R.id.textViewDate);
        RadioGroup date = (RadioGroup)findViewById(R.id.RadioGroupDate);
        String responce= getIntent().getStringExtra("Data");
        try {
            JSONObject data = new JSONObject(responce);
            JSONObject listCourse = data.getJSONObject("DataModel");
            txtViewCourseName.setText(listCourse.getString("Name"));
            txtViewCourseAbout.setText(listCourse.getString("AboutCourse"));
            JSONArray arrJson = listCourse.getJSONArray("DocumentsRequired");
            for(int i = 0; i < arrJson.length(); i++) {
                txtViewDocuments.setText(arrJson.getString(i)+"\n");
            }
            new DownloadImageTask((ImageView) findViewById(R.id.imageViewCourse))
                    .execute(listCourse.getString("Image"));
            JSONArray arrJsonDuration = listCourse.getJSONArray("DurationofCourse");
            for(int i = 0; i < arrJsonDuration.length(); i++) {
                txtViewDuration.setText(arrJsonDuration.getString(i)+"\n");
            }
            JSONArray arrJsonEligibilty = listCourse.getJSONArray("Eligibilty");
            for(int i = 0; i < arrJsonEligibilty.length(); i++) {
                txtViewEligibilty.setText(arrJsonEligibilty.getString(i)+"\n");
            }
            //for  schedule dates
            JSONArray arrJsonDates = listCourse.getJSONArray("CourseScheduleDetails");
            if(arrJsonDates.length()==0) {
                txtViewNoDates.setText("No dates are available");
                date.setVisibility(View.INVISIBLE);
            }
            else {
                Log.d(Utility.TAG, arrJsonDates.toString());
                txtViewNoDates.setVisibility(View.INVISIBLE);
                final String[] datearray = new String[arrJsonDates.length()];
                for (int i = 0; i <= arrJsonDates.length(); i++) {
                    dateData = new JSONObject(arrJsonDates.getString(i));
                    RadioButton rdbtn = new RadioButton(this);
                    rdbtn.setId(i);
                    rdbtn.setText(dateData.getString("CourseDate"));
                    Log.d(Utility.TAG, rdbtn.getText().toString());
                    datearray[i] = arrJsonDates.getString(i);
                    date.addView(rdbtn);
                    date.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup radioGroup, int i) {
                            courseDate = datearray[i];
                        }
                    });
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        myLoopjTask = new MyLoopjTask();
        myLoopjTask.delegte = this;
    }

    public void ApplyCourse(View view) {
        if(courseDate != null) {
            /*
            Intent i = new Intent(getApplicationContext(), FormActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(i);*/

            RequestParams requestParams = new RequestParams();
            try {
                SharedPreferences settings = getSharedPreferences(LoginActivity.PREFS_NAME, MODE_PRIVATE);
                requestParams.put("key",settings.getString(Utility.AuthToken, ""));
                requestParams.put("courseid", dateData.getString("ID"));
                requestParams.put("datetime", courseDate);
                requestParams.put("CourseFee", dateData.getString("CourseFee"));
                requestParams.put("SeatAvailable", dateData.getString("SeatAvaiable"));
                requestParams.put("isEarlyBirdDiscountEligible", dateData.getString("isEarlyBirdDiscountEligible"));
                requestParams.put("isPreviousStudent", dateData.getString("isPreviousStudent"));

            }catch (JSONException e) {
                e.printStackTrace();
            }
            showProgress(true);
            myLoopjTask.executeLoopjCallPost(requestParams,Utility.SetCourseInCartUrl);
        }
        else
            Toast.makeText(getApplicationContext(), "Select a course date to apply", Toast.LENGTH_LONG).show();
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        @Override
        protected void onPreExecute() {
            showProgress(false);
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = Utility.SERVER +urls[0];
            Bitmap mIcon11 = null;

            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
            showProgress(false);
        }

    }

    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void processFinish(String output) {

            String responseString = output;
        Log.d(Utility.TAG,responseString);
            try {
                JSONObject loginOutParams = new JSONObject(responseString);

                if (loginOutParams.getBoolean(Utility.ResponseSuccess)) {
                    Intent i = new Intent(getApplicationContext(), FormActivity.class).putExtra("Data", dateData.toString());
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(i);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        showProgress(false);
    }
}