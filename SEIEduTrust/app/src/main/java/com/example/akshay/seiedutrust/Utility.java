package com.example.akshay.seiedutrust;

/**
 * Created by Akshay on 29-05-2018.
 */

public class Utility {


   //Server Info
   public static String Primary_Server = "http://www.seiedutrust.com/";
   public static String Test_Server = "http://seiedutrust-001-site2.gtempurl.com/";
   public static String SERVER = Test_Server;

   //Rest Api Urls
   public  static String GetAllCoursesUrl = "Home/GetAllCourses/";
   public  static String GetCourseDetailUrl = "Home/GetCourseDetail/";
   public  static String LoginUrl = "Login/LogonFromDevice/";
   public  static String UploadImageUrl = "SaveFiles";
   public  static String ApplyFormUrl = "CreateAdmissionForm/";
   public  static String SetCourseInCartUrl = "Home/setCourseForCart/";

   //Response Messages
   public static String ResponseSuccess = "Success";
   public static String ResponseDataModel = "DataModel";
   public static String ResponseMessages = "Messages";

   //Other
   public static String OnSuccess = "onSuccess";
   public static String OnFailure = "onFailure";
   public static String AuthToken = "key";

   public static final String TAG = "SEIEduTrust";




}
