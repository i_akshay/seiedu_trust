package com.example.akshay.seiedutrust;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Akshay on 22-04-2018.
 */

public class MyLoopjTask {


    AsyncHttpClient asyncHttpClient;
    RequestParams requestParams;
    public AsyncInterface delegte = null;


    String jsonResponse;


    public MyLoopjTask() {
        asyncHttpClient = new AsyncHttpClient();
        requestParams = new RequestParams();
    }

    public void executeLoopjCall(String queryTerm) {
        requestParams.put("s", queryTerm);
        asyncHttpClient.get(Utility.SERVER, requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                jsonResponse = response.toString();

                Log.i(Utility.TAG, Utility.OnSuccess + jsonResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.e(Utility.TAG, Utility.OnFailure + errorResponse);
            }
        });
    }

    public void executeLoopjCallPost(  RequestParams  queryTerm , String relUrl) {

        asyncHttpClient.post(Utility.SERVER+relUrl,queryTerm,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                jsonResponse = response.toString();
                delegte.processFinish(jsonResponse);
                Log.i(Utility.TAG, "onSuccess: " + jsonResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.e(Utility.TAG, "onFailure: " + errorResponse);
            }
            @Override
            public  void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Log.e(Utility.TAG, "onFailure: " + responseString);
            }
        });
    }
    public void executeLoopjCallPostSecond(  RequestParams  queryTerm ,String relUrl) {

        asyncHttpClient.post(Utility.SERVER+relUrl,queryTerm,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                jsonResponse = response.toString();
                delegte.processFinish(jsonResponse);
                Log.i(Utility.TAG, "onSuccess: " + jsonResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.e(Utility.TAG, "onFailure: " + errorResponse);
            }

            @Override
            public  void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Log.e(Utility.TAG, "onFailure: " + responseString);
            }
        });
    }
}
