package com.example.akshay.seiedutrust;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.loopj.android.http.*;

import cz.msebera.android.httpclient.entity.mime.Header;

public class MainActivity extends AppCompatActivity implements AsyncInterface{


    AsyncHttpClient client;
    private int city;
    private  int course;

    private  boolean type = false,selectcourse=false;
    MyLoopjTask myLoopjTask;
    Spinner spinnerCoursesList;
    List<String> categories;
    JSONArray listCourse;
    private View mProgressView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mProgressView = findViewById(R.id.main_progress);
        // Spinner element
        Spinner spinnerCity = (Spinner) findViewById(R.id.spinnerCity);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.city, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinnerCity.setAdapter(adapter);


        final Spinner spinnerCourses = (Spinner) findViewById(R.id.spinnerCourseType);
        ArrayAdapter<CharSequence> Courseadapter = ArrayAdapter.createFromResource(this,
                R.array.courses, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        Courseadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinnerCourses.setAdapter(Courseadapter);

        spinnerCoursesList = (Spinner) findViewById(R.id.spinner_List);
        categories = new ArrayList<String>();
        categories.add("Select");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        // Specify the layout to use when the list of choices appears
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinnerCoursesList.setAdapter(dataAdapter);
        spinnerCoursesList.setActivated(false);
        String s= getIntent().getStringExtra("UserName");
        TextView txtView = (TextView) findViewById(R.id.textViewHello);
        txtView.append(s);
        // Spinner click listener
        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {

                String item = adapterView.getItemAtPosition(pos).toString();
                city = pos+1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Spiner Courses
        spinnerCourses.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                String item = adapterView.getItemAtPosition(pos).toString();
                course = pos;
                if(pos > 0 ){
                attempedDataRetrive(city,course);
                type = true;
                }else
                {
                    Toast.makeText(adapterView.getContext(), "Select Course" , Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Spiner Courses list
        spinnerCoursesList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                String item = adapterView.getItemAtPosition(pos).toString();
                course = pos;
                if(pos > 0 ){
                    attempedCourseSelect(pos);
                    selectcourse = true;
                    type = false;
                }else
                {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        myLoopjTask = new MyLoopjTask();
        myLoopjTask.delegte = this;
    }

    public void attempedDataRetrive(int institutetype,int coursetype){
        showProgress(true);
        RequestParams requestParams = new RequestParams();
        try {
            requestParams.put("institutetype",institutetype);
            requestParams.put("coursetype",coursetype);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //myLoopjTask.executeLoopjCall(CourseParams.toString());
        myLoopjTask.executeLoopjCallPost(requestParams,Utility.GetAllCoursesUrl);
    }

    public void   attempedCourseSelect(int id){
        showProgress(true);
        RequestParams requestParams = new RequestParams();
        try {
            JSONObject c = listCourse.getJSONObject(id-1);
            requestParams.put("courseid",c.getString("ID"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        //myLoopjTask.executeLoopjCall(CourseParams.toString());
        myLoopjTask.executeLoopjCallPost(requestParams,Utility.GetCourseDetailUrl);
    }

    @Override
    public void processFinish(String output) {
        if(type) {
            String responseString = output;
            try {
                JSONObject loginOutParams = new JSONObject(responseString);
                if (loginOutParams.getBoolean(Utility.ResponseSuccess)) {
                    finish(loginOutParams);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if(selectcourse) {
            String responseString = output;
            try {
                JSONObject loginOutParams = new JSONObject(responseString);
                if (loginOutParams.getBoolean(Utility.ResponseSuccess)) {
                    Intent i = new Intent(getApplicationContext(), CourseDetailActivity.class).putExtra("Data", responseString);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(i);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        showProgress(false);
    }


    public  void   finish(JSONObject data) {
        try {
            listCourse = data.getJSONArray(Utility.ResponseDataModel);
            categories.clear();
            categories.add("Select");
            for (int i = 0; i < listCourse.length(); i++) {
                JSONObject c = listCourse.getJSONObject(i);
                categories.add(c.getString("Name"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        // Specify the layout to use when the list of choices appears
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinnerCoursesList.setAdapter(dataAdapter);
        spinnerCoursesList.setActivated(true);
    }
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }
}
